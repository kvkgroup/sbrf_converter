<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

/**
 * UploadForm is the model behind the contact form.
 */
class UploadForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'ppt, pptx, pdf'],
        ];
    }

    public function attributeLabels() {
        return [
            'file' => 'Файл',
        ];
    }


    public function upload()
    {
        if ($this->validate()) {
            $path = 'uploads/'. date('YmdHis');
            FileHelper::createDirectory($path);
            $file_path = $path .'/' . $this->file->baseName . '.' . $this->file->extension;
            $this->file->saveAs($file_path);
            return $file_path;
        } else {
            return false;
        }
    }
}
