<?php
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UploadForm */
/* @var $local_pdf string|false */
/* @var $images array */

$this->title = 'Converter demo';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($model, 'file')->fileInput(); ?>

            <button type="submit" class="btn btn-info">Конвертировать</button>

            <?php ActiveForm::end() ?>
        </div>
        <hr/>
        <?php if ($local_pdf) {?>
            <div class="row">
                <?= \yii\helpers\Html::a('Сконвертированный pdf', $local_pdf, ['target'=>'_blank']); ?>
            </div>
            <hr/>
        <?php } ?>
        <?php if ($images) {?>
            <div class="row">
                <?php foreach ($images as $img) {?>
                    <a data-fancybox="gallery" href="<?= $img; ?>" class="col-lg-4"><img src="<?= $img; ?>" class="img-thumbnail" /></a>
                <?php } ?>
            </div>
            <hr/>
        <?php } ?>
    </div>
</div>