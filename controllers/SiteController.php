<?php

namespace app\controllers;

use app\helpers\Converter;
use app\models\UploadForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new UploadForm();
        $local_pdf = false;
        $images = [];
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate() && $file = $model->upload()) {
                $file_parts = pathinfo($file);
                if($file_parts['extension']=='pdf') {
                    $pdf_full_path = \Yii::getAlias('@webroot').'/'.$file;
                } else {
                    $presentation_full_path = \Yii::getAlias('@webroot').'/'.$file;
                    $pdf_full_path = Converter::presentationToPdf($presentation_full_path);
                    if ($pdf_full_path) $local_pdf = str_replace(\Yii::getAlias('@webroot'), '', $pdf_full_path);
                }

                if ($pdf_full_path) {
                    $images_arr = Converter::pdfToImages($pdf_full_path);
                    foreach ($images_arr as $one_image) {
                        $images[] = str_replace(\Yii::getAlias('@webroot'), '', $one_image);;
                    }
                }
            }
        }

        return $this->render('index',
        [
            'model' => $model,
            'local_pdf' => $local_pdf,
            'images' => $images
        ]);
    }
}
