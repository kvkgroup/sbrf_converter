<?php

namespace app\helpers;

/**
 * Класс для конвертации ppt/pptx в pdf и pdf в картинки
 *
 */
class Converter
{

    /**
     * Обработка ошибок класса.
     *
     * @param string $message
     *
     * @return false
     */
    private static function _riseError($message)
    {
        error_log($message);
        return false;
    }

    /**
     * Конвертировать ppt/pptx в pdf
     *
     * @param string $file полный путь к файлу презентации
     *
     * @return string|false полный путь к файлу pdf или false в случае ошибки
     */
    public static function presentationToPdf($file)
    {
        $result = false;

        if (file_exists($file)) {
            $output = [];
            $retval = null;

            exec("unoconv -f pdf '" . $file . "'", $output, $retval);
            if ($retval === 0) {
                //при успешной конвертации возвращаем путь к pdf файлу
                $result = substr_replace($file, 'pdf', strrpos($file, '.') + 1);
            } else {
                //при неуспешной конвертации записываем вывод в ошибки
                self::_riseError(print_r($output, true));
            }
        } else {
            self::_riseError("Файл `{$file}` не существует");
        }

        return $result;
    }

    /**
     * Конвертировать pdf в картинки
     *
     * @param string $file полный путь к файлу pdf
     * @param int $image_quality качество картинки
     *
     * @return array массив полных путей к картинкам
     */
    public static function pdfToImages($file, $image_quality = 90)
    {
        $result = [];
        if (file_exists($file)) {
            $images_path = substr_replace($file, '', strrpos($file, '/') + 1) . 'images/';
            if (!file_exists($images_path)) {
                if (!mkdir($images_path, 0755)) {
                    self::_riseError("Не удалось создать папку `{$images_path}`");
                    return $result;
                }
            }

            try {
                //Получаем информацию о файле
                $im = new \imagick($file);
                $params = $im->identifyImage();

                //Сохраняем картинки
                $imagick = new \imagick();
                $imagick->setPage($params['geometry']['width'], $params['geometry']['height'], 0, 0);
                $imagick->setResolution($params['resolution']['x'], $params['resolution']['y']);
                $imagick->readImage($file);
                $imagick->setImageUnits(\imagick::RESOLUTION_PIXELSPERINCH);
                $imagick->setImageFormat("jpg");
                $imagick->setImageCompression(\imagick::COMPRESSION_JPEG);
                $imagick->setImageCompressionQuality($image_quality);
                foreach ($imagick as $i => $imagick_image) {
                    $one_image_path = $images_path . ($i + 1) . ".jpg";
                    $imagick_image->writeImage($one_image_path);
                    $result[] = $one_image_path;
                }
                $imagick->clear();
            } catch (\Throwable $throwable) {
                self::_riseError($throwable->getMessage());
            }
        } else {
            self::_riseError("Файл `{$file}` не существует");
        }

        return $result;
    }
}